/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WORLD_H
#define WORLD_H

#include "chunk.h"
#include <vector>
#include <atomic>

class World {
    public:
        World(int width, int height, int chunkWidth, int chunkHeight);
        virtual void tick() = 0; //make sequential and concurrent version
        void setCell(int x, int y, bool alive);
        int isAlive(int x, int y);
        int getWidth();
        int getHeight();
    protected:
        void establishNeighborhoods();
        Chunk& getChunkContaining(int x, int y);
        std::atomic_long currentStep;
        int chunkWidth, chunkHeight;
        int worldWidth, worldHeight;
        int chunksX, chunksY; //amount of chunks in orientation
        std::vector<Chunk> chunks;
        friend class Renderer;
};

#endif

