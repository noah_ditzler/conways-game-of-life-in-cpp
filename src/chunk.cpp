/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include "chunk.h"

Direction getOppositeDirection(Direction d) {
    switch (d) {
        case N:
            return S;
        case NE:
            return SW;
        case E:
            return W;
        case SE:
            return NW;
        case S:
            return N;
        case SW:
            return NE;
        case W:
            return E;
        case NW:
            return SE;
        default:
            //won't occur but appeases the compiler
            return E;
    }
}

std::pair<int, int> getDirectionOffset(Direction d) {
    switch (d) {
        case N:
            return std::pair(0, -1);
        case NE:
            return std::pair(1, -1);
        case E:
            return std::pair(1, 0);
        case SE:
            return std::pair(1, 1);
        case S:
            return std::pair(0, 1);
        case SW:
            return std::pair(-1, 1);
        case W:
            return std::pair(-1, 0);
        case NW:
            return std::pair(-1, -1);
        default:
            return std::pair(0, 0);
    }
}

Cell Chunk::worldborder;

Cell::Cell() {
    this->alive= 0;
    this->livingNeighbors= 0;
}

Chunk::Chunk(int width, int height, int absX, int absY) {
    currentStep = 0;
    neighborCount = 0;
    this->width = width;
    this->height = height;
    this->absX = absX;
    this->absY = absY;
    this->expectRenderer = 0;
    //the area that's simulated is from (inclusive) 1 1 to width - 2, height - 2
    //(border is not simulated
    this->area = new Cell[(width + 2) * (height + 2)];
    this->neighbors = new Chunk*[8]{0};
    this->borders = new Cell**[8];
    this->borders[N] = new Cell*[width];
    this->borders[NE] = new Cell*[1];
    this->borders[E] = new Cell*[height];
    this->borders[SE] = new Cell*[1];
    this->borders[S] = new Cell*[width];
    this->borders[SW] = new Cell*[1];
    this->borders[W] = new Cell*[height];
    this->borders[NW] = new Cell*[1];
    this->lock = new std::shared_mutex();
    this->doneTicking = new std::condition_variable_any();
    this->countDoneSyncing = new std::atomic_int();
    (*countDoneSyncing) = 0;
}

Chunk::~Chunk() {
    delete[] area;
    delete[] neighbors;
    for (int i = 0; i < 8; i++) {
        delete[] borders[i];
    }
    delete[] borders;
    delete lock;
    delete doneTicking;
    delete countDoneSyncing;
}

int Chunk::getBorderLength(Direction d) {
    switch (d) {
        case N:
        case S:
            return width;
        case E:
        case W:
            return height;
        default:
            return 1;
    }
}

void Chunk::setWorldborderNeighbor(Direction d) {
    for (int i = 0; i < getBorderLength(d); i++) {
        borders[d][i] = &worldborder;
    }
}

//Direction is direction in which the c lies from this
void Chunk::setNeighbor(Chunk* c, Direction d) {
    if (neighbors[d])
        return;
    int startX = 1;
    int startY = 1;
    int stepX = 0;
    int stepY = 0;
    switch (d) {
        case N:
            startY = height;
            stepX = 1;
            break;
        case NE:
            startY = height;
            break;
        case E:
            stepY = 1;
            break;
        case SE:
            break;
        case S:
            stepX = 1;
            break;
        case SW:
            startX = width;
            break;
        case W:
            startX = width;
            stepY = 1;
            break;
        case NW:
            startX = width;
            startY = height;
            break;
    }

    for (int i = 0; i < getBorderLength(d); i++) {
        borders[d][i] = c->area + (startX + (i * stepX) + (width + 2) * (startY + (i * stepY)));
    }

    neighbors[d] = c;
    neighborCount++;
}

void Chunk::tickCells() {
    int neighborsDone = countDoneSyncing->load();
    while (neighborsDone < neighborCount + expectRenderer) {
        countDoneSyncing->wait(neighborsDone);
        neighborsDone = countDoneSyncing->load();
    }
    std::unique_lock l(*lock);
    for (int y = 1; y <= height; y++) {
        for (int x = 1; x <= width; x++) {
            Cell& center = area[x + y * (width + 2)];
            center.livingNeighbors = 0;

            center.livingNeighbors += area[x + (y - 1) * (width + 2)].alive ? 1 : 0;
            center.livingNeighbors += area[(x + 1) + (y - 1) * (width + 2)].alive ? 1 : 0;
            center.livingNeighbors += area[(x + 1) + y * (width + 2)].alive ? 1 : 0;
            center.livingNeighbors += area[(x + 1) + (y + 1) * (width + 2)].alive ? 1 : 0;

            center.livingNeighbors += area[x + (y + 1) * (width + 2)].alive ? 1 : 0;
            center.livingNeighbors += area[(x - 1) + (y + 1) * (width + 2)].alive ? 1 : 0;
            center.livingNeighbors += area[(x - 1) + y * (width + 2)].alive ? 1 : 0;
            center.livingNeighbors += area[(x - 1) + (y - 1) * (width + 2)].alive ? 1 : 0;
        }
    }
    for (int y = 1; y <= height; y++) {
        for (int x = 1; x <= width; x++) {
            Cell& cell = area[x + y * (width + 2)];
            cell.alive = cell.livingNeighbors == 3
                || (cell.alive && cell.livingNeighbors == 2);
        }
    }
    (*countDoneSyncing) = 0;
    currentStep++;
    doneTicking->notify_all();
}

void Chunk::setCell(int x, int y, bool alive) {
    int relX = 1 + x - absX;
    int relY = 1 + y - absY;
    std::unique_lock l(*lock);
    area[relX + relY * (width + 2)].alive = alive;
}

bool Chunk::isAlive(int x, int y) {
    int relX = 1 + x - absX;
    int relY = 1 + y - absY;
    std:shared_lock l(*lock);
    return area[relX + relY * (width + 2)].alive;
}

void Chunk::synchronizeBorder() {
    {//N
        if (neighbors[N]) {
            std::shared_lock l(*(neighbors[N]->lock));
            //locking to protect this->currentStep
            lock->lock_shared();
            while (currentStep != neighbors[N]->currentStep) {
                lock->unlock_shared();
                neighbors[N]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        for (int i = 0; i < width; i++) {
            area[1 + i] = *(borders[N][i]);
        }
        if (neighbors[N]) {
            (*(neighbors[N]->countDoneSyncing))++;
            neighbors[N]->countDoneSyncing->notify_one();
        }
    }
    {//S
        if (neighbors[S]) {
            std::shared_lock l(*(neighbors[S]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[S]->currentStep) {
                lock->unlock_shared();
                neighbors[S]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        for (int i = 0; i < width; i++) {
            area[(width + 2) * (height + 1) + 1 + i] = *(borders[S][i]);
        }
        if (neighbors[S]) {
            (*(neighbors[S]->countDoneSyncing))++;
            neighbors[S]->countDoneSyncing->notify_one();
        }
    }
    {//W
        if (neighbors[W]) {
            std::shared_lock l(*(neighbors[W]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[W]->currentStep) {
                lock->unlock_shared();
                neighbors[W]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        for (int i = 0; i < height; i++) {
            area[(width + 2) * (i + 1)] = *(borders[W][i]);
        }
        if (neighbors[W]) {
            (*(neighbors[W]->countDoneSyncing))++;
            neighbors[W]->countDoneSyncing->notify_one();
        }
    }
    {//E
        if (neighbors[E]) {
            std::shared_lock l(*(neighbors[E]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[E]->currentStep) {
                lock->unlock_shared();
                neighbors[E]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        for (int i = 0; i < height; i++) {
            area[(width + 2) * (i + 1) + 1 + width] = *(borders[E][i]);
        }
        if (neighbors[E]) {
            (*(neighbors[E]->countDoneSyncing))++;
            neighbors[E]->countDoneSyncing->notify_one();
        }
    }
    {//NW
        if (neighbors[NW]) {
            std::shared_lock l(*(neighbors[NW]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[NW]->currentStep) {
                lock->unlock_shared();
                neighbors[NW]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        area[0] = **(borders[NW]);
        if (neighbors[NW]) {
            (*(neighbors[NW]->countDoneSyncing))++;
            neighbors[NW]->countDoneSyncing->notify_one();
        }
    }
    {//NE
        if (neighbors[NE]) {
            std::shared_lock l(*(neighbors[NE]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[NE]->currentStep) {
                lock->unlock_shared();
                neighbors[NE]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        area[width + 1] = **(borders[NE]);
        if (neighbors[NE]) {
            (*(neighbors[NE]->countDoneSyncing))++;
            neighbors[NE]->countDoneSyncing->notify_one();
        }
    }
    {//SE
        if (neighbors[SE]) {
            std::shared_lock l(*(neighbors[SE]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[SE]->currentStep) {
                lock->unlock_shared();
                neighbors[SE]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        area[(width + 2) * (height + 2) - 1] = **(borders[SE]);
        if (neighbors[SE]) {
            (*(neighbors[SE]->countDoneSyncing))++;
            neighbors[SE]->countDoneSyncing->notify_one();
        }
    }
    {//SW
        if (neighbors[SW]) {
            std::shared_lock l(*(neighbors[SW]->lock));
            lock->lock_shared();
            while (currentStep != neighbors[SW]->currentStep) {
                lock->unlock_shared();
                neighbors[SW]->doneTicking->wait(l);
                lock->lock_shared();
            }
            lock->unlock_shared();
        }
        area[(width + 2) * (height + 1)] = **(borders[SW]);
        if (neighbors[SW]) {
            (*(neighbors[SW]->countDoneSyncing))++;
            neighbors[SW]->countDoneSyncing->notify_one();
            
        }
    }
}

