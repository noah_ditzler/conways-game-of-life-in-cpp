/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "renderer.h"

Renderer::Renderer(World* world) {
    this->world = world;
    this->chunks = &(world->chunks);
    this->frame = new bool[world->getWidth() * world->getHeight()];
    currentStep = 0;
    runRenderer = true;
    countDoneCopying = 0;
}

Renderer::~Renderer() {
    delete[] frame;
}

void Renderer::runCopierWorker(Chunk* c) {
    long step = 0;

    for(;;) {
    //wait for doneTicking here
    std::shared_lock l(*(c->lock));
    if (step) {
        while (runRenderer && step > c->getCurrentStep()) {
            c->doneTicking->wait(l);
        }
        if (!runRenderer)
            return;
    }
    lock.lock_shared();
    for (int y = 0; y < c->height; y++) {
        for (int x = 0; x < c->width; x++) {
            frame[x + c->absX + (y + c->absY) * world->getWidth()]
                = c->area[(x + 1) + ((y + 1) * (c->width + 2))].alive;
        }
    }
    lock.unlock_shared();

    countDoneCopying++;
    countDoneCopying.notify_all();
    step++;
    while (runRenderer && countDoneCopying > 0) {
        countDoneCopying.wait(countDoneCopying);
    }
    if (!runRenderer)
        return;
    (*(c->countDoneSyncing))++;
    c->countDoneSyncing->notify_one();
}
}

void Renderer::runRenderWorker() {
    for (;;) {
        unsigned long doneCopying = countDoneCopying;
        while (runRenderer && doneCopying < chunks->size()) {
            countDoneCopying.wait(doneCopying);
            doneCopying = countDoneCopying;
        }
        if (!runRenderer)
            return;
        lock.lock();
        renderFrame();
        lock.unlock();

        currentStep++;
        countDoneCopying = 0;
        countDoneCopying.notify_all();
    }
}

void Renderer::startWorkers() {
    for (std::vector<Chunk>::size_type i = 0; i < chunks->size(); i++) {
        (*chunks)[i].expectRenderer = 1;
        copiers.emplace_back(&Renderer::runCopierWorker, this, &((*chunks)[i]));
    }
    renderThread = std::thread(&Renderer::runRenderWorker, this);

}

void Renderer::stopWorkers() {
    runRenderer = false;
    countDoneCopying++;
    countDoneCopying.notify_all();
    renderThread.join();

    for (unsigned int i = 0; i < chunks->size(); i++) {
        (*chunks)[i].doneTicking->notify_all();
    }
    for (auto& t : copiers) {
        t.join();
    }
}

