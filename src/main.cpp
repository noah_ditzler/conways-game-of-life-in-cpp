/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <stdio.h>
#include "world.h"
#include "chunk.h"
#include "sequential_world.h"
#include "concurrent_world.h"
#include "renderer.h"
#include "ascii_renderer.h"

int main(int argc, char** argv) {
    int worldWidth = 20;
    int worldHeight = 20;
    ConcurrentWorld world(worldWidth, worldHeight, 10, 10);//TODO variable chunk size

    for (;;) {
        std::cout << "Enter x and y coordinates of a cell you want to toggle, or 'run':" << std::endl;
        std::string in;
        std::getline(std::cin, in);
        if (in == "run")
            break;
        else {
            std::string::size_type splitter = in.find(" ");
            unsigned int x, y;
            try {
                x = std::stoi(in.substr(0, splitter));
                y = std::stoi(in.substr(splitter));
            } catch (std::invalid_argument const& e) {
                std::cout << "Invalid input. Try again:" << std::endl;
                continue;
            } catch (std::out_of_range const& e) {
                std::cout << "Invalid input. Try again:" << std::endl;
                continue;
            }
            if (x >= worldWidth || y >= worldHeight) {
                std::cout << "Invalid input, try again:" << std::endl;
            }
            bool alive = world.isAlive(x, y);
            world.setCell(x, y, !alive);
            std::cout << "Set Cell at " << x << ' ' << y << " to ";
            if (alive) {
                std::cout << "dead." << std::endl;
            } else {
                std::cout << "alive." << std::endl;
            }
        }
    }
    AsciiRenderer renderer(&world);
    renderer.startWorkers();
    world.startChunkWorkers();
    for (;;) {
        std::string in;
        std::getline(std::cin, in);
        world.tick();
    }
    world.stopChunkWorkers();
    renderer.stopWorkers();
    return 0;
}

