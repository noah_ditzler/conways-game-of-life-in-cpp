/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "concurrent_world.h"

void ConcurrentWorld::runChunk(Chunk* c) {
    c->synchronizeBorder();
    for (;;) {
        long ccs = c->getCurrentStep();
        while (currentStep <= ccs) {
            //while chunk is ahead, wait
            currentStep.wait(ccs);
        }
        if (!running && currentStep == ccs + 1) {
            c->wakeWaiters();
            return;
        }
        c->tickCells();
        c->synchronizeBorder();
    }
}

ConcurrentWorld::ConcurrentWorld(int width, int height, int chunkWidth, int chunkHeight):World(width, height, chunkWidth, chunkHeight) {
        running = true;
}
void ConcurrentWorld::startChunkWorkers() {
    threads.reserve(chunksX * chunksY);
    for (int i = 0; i < chunksX * chunksY; i++) {
        threads.emplace_back(&ConcurrentWorld::runChunk, this, &(chunks[i]));
    }
}


void ConcurrentWorld::stopChunkWorkers() {
    running = false;
    currentStep++; //wake up workers
    currentStep.notify_all();
    for (auto &t : threads) {
        t.join();
    }
}

void ConcurrentWorld::tick() {
    currentStep++;
    currentStep.notify_all();
}

