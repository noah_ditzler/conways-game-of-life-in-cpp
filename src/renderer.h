/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RENDERER_H
#define RENDERER_H

#include "chunk.h"
#include "world.h"
#include <vector>
#include <thread>
#include <atomic>
#include <shared_mutex>

class Renderer {
    public:
        Renderer(World* world);
        ~Renderer();
        virtual void renderFrame() = 0;
        void startWorkers();
        //must stop chunks first and start them second
        void stopWorkers();
    protected:
        void stopCopierWorkers();
        void startRenderWorker();
        void runCopierWorker(Chunk* c);
        void runRenderWorker();
        std::atomic_long currentStep;
        std::atomic_uint countDoneCopying;
        std::vector<std::thread> copiers;
        std::atomic_bool runRenderer;
        std::thread renderThread;
        std::vector<Chunk>* chunks;
        World* world;
        bool* frame;
        std::shared_mutex lock;
};

#endif

