/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHUNK_H
#define CHUNK_H

#include <utility>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <atomic>

enum Direction {
    N, NE, E, SE, S, SW, W, NW
};
Direction getOppositeDirection(Direction d);
std::pair<int, int> getDirectionOffset(Direction d);

struct Cell {
    bool alive;
    char livingNeighbors;
    Cell();
};

class Chunk {
    public:
        //absX and absY are the absolute coords of the cell corresponding to 1 1
        //in the chunk (top left of those that are ticked)
        Chunk(int width, int height, int absX, int absY);
        Chunk(const Chunk&) = delete;
        Chunk(Chunk&&) = default;
        ~Chunk();

        void setNeighbor(Chunk* neighbor, Direction d);
        void setWorldborderNeighbor(Direction d);
        void setCell(int x, int y, bool alive);
        bool isAlive(int x, int y);
        void tickCells();
        void synchronizeBorder();
        long getCurrentStep() {std::shared_lock l(*lock); return currentStep;}
        void wakeWaiters() {doneTicking->notify_all();}

    private:
        int getBorderLength(Direction d);
        static Cell worldborder;
        long currentStep;
        std::condition_variable_any* doneTicking;
        std::shared_mutex* lock;
        std::atomic_int* countDoneSyncing;
        int width, height;
        int absX, absY; //absolute position of area's tile at 1 1
        Cell* area;
        //order: N NE E SE S SW W NW
        int neighborCount;
        Chunk** neighbors;
        //same order as neighbors
        Cell*** borders;
        char expectRenderer; //1 or 0

        friend class Renderer;
};


#endif

