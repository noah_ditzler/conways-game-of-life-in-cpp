/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "world.h"

World::World(int width, int height, int chunkWidth, int chunkHeight) {
    if (width % chunkWidth && height % chunkHeight) {
        //TODO error handling
    }
    currentStep = 0;
    worldWidth = width;
    worldHeight = height;
    this->chunkWidth = chunkWidth;
    this->chunkHeight = chunkHeight;
    chunksX = width / chunkWidth;
    chunksY = height / chunkHeight;
    chunks = std::vector<Chunk>();
    chunks.reserve(chunksX * chunksY);
    for (int y = 0; y < chunksY; y++) {
        for (int x = 0; x < chunksX; x++) {
            chunks.emplace_back(chunkWidth, chunkHeight, x * chunkWidth, y * chunkHeight);
        }
    }
    establishNeighborhoods();
}

void World::establishNeighborhoods() {
    for (int y = 0; y < chunksY; y++) {
        for (int x = 0; x < chunksX; x++) {
            Chunk* c = &(chunks[x + y * chunksX]);
            for (int i = 0; i < 8; i++) {
                Direction d = (Direction) i;
                auto offset = getDirectionOffset(d);
                int neighborX = x + offset.first;
                int neighborY = y + offset.second;
                if (neighborX < 0 || neighborY < 0 || neighborX >= chunksX || neighborY >= chunksY) {
                    c->setWorldborderNeighbor(d);
                } else {
                    Chunk* cn = &(chunks[neighborX + neighborY * chunksX]);
                    c->setNeighbor(cn, d);
                }
            }
        }
    }
}

Chunk& World::getChunkContaining(int x, int y) {
    int cX = x / chunkWidth;
    int cY = y / chunkHeight;
    return chunks[cX + cY * chunksX];
}

void World::setCell(int x, int y, bool alive) {
    getChunkContaining(x, y).setCell(x, y, alive);
}

int World::isAlive(int x, int y) {
    return getChunkContaining(x, y).isAlive(x, y);
}

int World::getWidth() {
    return worldWidth;
}

int World::getHeight() {
    return worldHeight;
}

