/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ascii_renderer.h"
#include <iostream> 

void AsciiRenderer::renderFrame() {
    for (int y = 0; y < world->getHeight(); y++) {
        for (int x = 0; x < world->getWidth(); x++) {
            if (frame[x + y * world->getWidth()]) {
                std::cout << "O";
            } else {
                std::cout << "_";
            }
        }
        std::cout << std::endl;
    }
    std::cout << "Step " << currentStep << ". Press enter to continue " << std::endl;
}

