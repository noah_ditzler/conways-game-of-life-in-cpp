# Conway's Game of Life

This is an implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_game_of_life) in C++. The Cells are grouped together in chunks which run concurrently.
It features a command line interface with ascii graphics.

## Example of usage:
```
Enter x and y coordinates of a cell you want to toggle, or 'run':
10 11
Set Cell at 10 11 to alive.
Enter x and y coordinates of a cell you want to toggle, or 'run':
9 11
Set Cell at 9 11 to alive.
Enter x and y coordinates of a cell you want to toggle, or 'run':
11 11
Set Cell at 11 11 to alive.
Enter x and y coordinates of a cell you want to toggle, or 'run':
run
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
_________OOO________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
Step 0. Press enter to continue 

____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
__________O_________
__________O_________
__________O_________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
Step 1. Press enter to continue 

____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
_________OOO________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
____________________
Step 2. Press enter to continue 
```

## Building and Running

Build by runing make.
You can then run the program with ./gol

To run tests, run make test.
This requires you to have catch2 installed.
On arch derived systems you can install it with `sudo pacman -Ss catch2`
Only linux is supported, if it works on other systems that's by luck.

## License and Copyright

Conway's game of life in C++
Copyright (C) 2023 Noah Ditzler

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

