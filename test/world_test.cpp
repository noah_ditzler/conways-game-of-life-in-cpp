/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

TEST_CASE("Sequential world", "[sequential_world]") {
    SequentialWorld world = SequentialWorld(180, 180, 18, 18);
    world.setCell(30, 30, true);
    world.setCell(30, 31, true);
    world.setCell(30, 32, true);

    REQUIRE(world.isAlive(30, 30));
    REQUIRE(world.isAlive(30, 31));
    REQUIRE(world.isAlive(30, 32));

    world.tick();

    REQUIRE(world.isAlive(29, 31));
    REQUIRE(world.isAlive(30, 31));
    REQUIRE(world.isAlive(31, 31));

    world.tick();

    REQUIRE(world.isAlive(30, 30));
    REQUIRE(world.isAlive(30, 31));
    REQUIRE(world.isAlive(30, 32));
}

TEST_CASE("Sequential world communication works", "[sequential_world]") {
    SequentialWorld world = SequentialWorld(10, 10, 1, 1);
    world.setCell(1, 0, true);
    world.setCell(1, 1, true);
    world.setCell(1, 2, true);

    REQUIRE(world.isAlive(1, 0));
    REQUIRE(world.isAlive(1, 1));
    REQUIRE(world.isAlive(1, 2));

    world.tick();

    REQUIRE(world.isAlive(0, 1));
    REQUIRE(world.isAlive(1, 1));
    REQUIRE(world.isAlive(2, 1));

    world.tick();

    REQUIRE(world.isAlive(1, 0));
    REQUIRE(world.isAlive(1, 1));
    REQUIRE(world.isAlive(1, 2));
}

TEST_CASE("Concurrent world", "[concurrent_world]") {
    ConcurrentWorld world = ConcurrentWorld(10, 10, 1, 1);

    world.setCell(1, 0, true);
    world.setCell(1, 1, true);
    world.setCell(1, 2, true);

    world.startChunkWorkers();

    world.tick();


    world.stopChunkWorkers();
    
    REQUIRE(world.isAlive(0, 1));
    REQUIRE(world.isAlive(1, 1));
    REQUIRE(world.isAlive(2, 1));


}

