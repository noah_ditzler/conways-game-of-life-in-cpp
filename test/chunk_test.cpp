/*
 * Conway's game of life in C++
*  Copyright (C) 2023 Noah Ditzler
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

TEST_CASE("Single chunk, single living cell dies", "[chunk]") {
    Chunk c(3, 3, 0, 0);
    for (int i = 0; i < 8; i++) {
        c.setWorldborderNeighbor((Direction) i);
    }
    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));

    REQUIRE(!c.isAlive(0, 1));
    REQUIRE(!c.isAlive(1, 1));
    REQUIRE(!c.isAlive(2, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(!c.isAlive(1, 2));
    REQUIRE(!c.isAlive(2, 2));

    c.setCell(1, 1, true);
    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));

    REQUIRE(!c.isAlive(0, 1));
    REQUIRE(c.isAlive(1, 1));
    REQUIRE(!c.isAlive(2, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(!c.isAlive(1, 2));
    REQUIRE(!c.isAlive(2, 2));

    c.tickCells();

    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));

    REQUIRE(!c.isAlive(0, 1));
    REQUIRE(!c.isAlive(1, 1));
    REQUIRE(!c.isAlive(2, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(!c.isAlive(1, 2));
    REQUIRE(!c.isAlive(2, 2));

}

TEST_CASE("Single chunk, blinker blinks", "[chunk]") {
    Chunk c(3, 3, 0, 0);
    for (int i = 0; i < 8; i++) {
        c.setWorldborderNeighbor((Direction) i);
    }
    c.setCell(0, 1, true);
    c.setCell(1, 1, true);
    c.setCell(2, 1, true);

    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));

    REQUIRE(c.isAlive(0, 1));
    REQUIRE(c.isAlive(1, 1));
    REQUIRE(c.isAlive(2, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(!c.isAlive(1, 2));
    REQUIRE(!c.isAlive(2, 2));

    c.tickCells();

    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));

    REQUIRE(!c.isAlive(0, 1));
    REQUIRE(c.isAlive(1, 1));
    REQUIRE(!c.isAlive(2, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(c.isAlive(1, 2));
    REQUIRE(!c.isAlive(2, 2));

    c.tickCells();

    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));

    REQUIRE(c.isAlive(0, 1));
    REQUIRE(c.isAlive(1, 1));
    REQUIRE(c.isAlive(2, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(!c.isAlive(1, 2));
    REQUIRE(!c.isAlive(2, 2));
}

TEST_CASE("Single chunk, block stays", "[chunk]") {
    Chunk c(4, 4, 0, 0);
    for (int i = 0; i < 8; i++) {
        c.setWorldborderNeighbor((Direction) i);
    }
    c.setCell(1, 1, true);
    c.setCell(1, 2, true);
    c.setCell(2, 1, true);
    c.setCell(2, 2, true);

    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));
    REQUIRE(!c.isAlive(3, 0));

    REQUIRE(!c.isAlive(0, 1));
    REQUIRE(c.isAlive(1, 1));
    REQUIRE(c.isAlive(2, 1));
    REQUIRE(!c.isAlive(3, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(c.isAlive(1, 2));
    REQUIRE(c.isAlive(2, 2));
    REQUIRE(!c.isAlive(3, 2));
    
    REQUIRE(!c.isAlive(0, 3));
    REQUIRE(!c.isAlive(1, 3));
    REQUIRE(!c.isAlive(2, 3));
    REQUIRE(!c.isAlive(3, 3));

    c.tickCells();

    REQUIRE(!c.isAlive(0, 0));
    REQUIRE(!c.isAlive(1, 0));
    REQUIRE(!c.isAlive(2, 0));
    REQUIRE(!c.isAlive(3, 0));

    REQUIRE(!c.isAlive(0, 1));
    REQUIRE(c.isAlive(1, 1));
    REQUIRE(c.isAlive(2, 1));
    REQUIRE(!c.isAlive(3, 1));

    REQUIRE(!c.isAlive(0, 2));
    REQUIRE(c.isAlive(1, 2));
    REQUIRE(c.isAlive(2, 2));
    REQUIRE(!c.isAlive(3, 2));
    
    REQUIRE(!c.isAlive(0, 3));
    REQUIRE(!c.isAlive(1, 3));
    REQUIRE(!c.isAlive(2, 3));
    REQUIRE(!c.isAlive(3, 3));
}

TEST_CASE("Two chunks interact", "[chunk]") {
    Chunk c1(3, 3, 0, 0);
    Chunk c2(3, 3, 3, 0);
    for (int i = 0; i < 8; i++) {
        if ((Direction) i != E) {
            c1.setWorldborderNeighbor((Direction) i);
        }
        if ((Direction) i != W) {
            c2.setWorldborderNeighbor((Direction) i);
        }
    }
    c1.setNeighbor(&c2, E);
    c2.setNeighbor(&c1, W);
    c1.setCell(1, 1, true);
    c1.setCell(2, 1, true);
    c2.setCell(3, 1, true);

    c1.synchronizeBorder();
    c2.synchronizeBorder();
    c1.tickCells();
    c2.tickCells();

    REQUIRE(c1.isAlive(2, 0));
    REQUIRE(c1.isAlive(2, 1));
    REQUIRE(c1.isAlive(2, 2));

    REQUIRE(!c1.isAlive(1, 1));
    REQUIRE(!c2.isAlive(3, 1));

    c1.synchronizeBorder();
    c2.synchronizeBorder();
    c1.tickCells();
    c2.tickCells();

    REQUIRE(c1.isAlive(1, 1));
    REQUIRE(c1.isAlive(2, 1));
    REQUIRE(c2.isAlive(3, 1));

    REQUIRE(!c1.isAlive(2, 0));
    REQUIRE(!c1.isAlive(2, 2));
    //TODO test North/South and corners
}

TEST_CASE("Nine chunks interact", "[chunk]") {
    int size = 3;
    Chunk cN(size, size, size, 0);
    Chunk cNE(size, size, 2 * size, 0);
    Chunk cE(size, size, 2 * size, size);

    Chunk cSE(size, size, 2 * size, 2 * size);
    Chunk cS(size, size, size, 2 * size);
    Chunk cSW(size, size, 0, 2 * size);

    Chunk cW(size, size, 0, size);
    Chunk cNW(size, size, 0, 0);
    Chunk cC(size, size, size, size);

    //establish neighborhood with center
    cC.setNeighbor(&cN, N);
    cN.setNeighbor(&cC, S);

    cC.setNeighbor(&cNE, NE);
    cNE.setNeighbor(&cC, SW);

    cC.setNeighbor(&cE, E);
    cE.setNeighbor(&cC, W);

    cC.setNeighbor(&cSE, SE);
    cSE.setNeighbor(&cC, NW);

    cC.setNeighbor(&cS, S);
    cS.setNeighbor(&cC, N);

    cC.setNeighbor(&cSW, SW);
    cSW.setNeighbor(&cC, NE);

    cC.setNeighbor(&cW, W);
    cW.setNeighbor(&cC, E);

    cC.setNeighbor(&cNW, NW);
    cNW.setNeighbor(&cC, SE);

    //establish neighborhoods among side tiles
    cN.setNeighbor(&cNE, E);
    cNE.setNeighbor(&cN, W);
    cN.setNeighbor(&cE, SE);
    cE.setNeighbor(&cN, NW);

    cNE.setNeighbor(&cE, S);
    cE.setNeighbor(&cNE, N);

    cE.setNeighbor(&cSE, S);
    cSE.setNeighbor(&cE, N);
    cE.setNeighbor(&cS, SW);
    cS.setNeighbor(&cE, NE);

    cSE.setNeighbor(&cS, W);
    cS.setNeighbor(&cSE, E);

    cS.setNeighbor(&cSW, W);
    cSW.setNeighbor(&cS, E);
    cS.setNeighbor(&cW, NW);
    cW.setNeighbor(&cS, SE);

    cSW.setNeighbor(&cW, N);
    cW.setNeighbor(&cSW, S);

    cW.setNeighbor(&cNW, N);
    cNW.setNeighbor(&cW, S);
    cW.setNeighbor(&cN, NE);
    cN.setNeighbor(&cW, SW);

    cNW.setNeighbor(&cN, E);
    cN.setNeighbor(&cNW, W);

    //put the worldborder
    cN.setWorldborderNeighbor(N);
    cN.setWorldborderNeighbor(NE);
    cN.setWorldborderNeighbor(NW);

    cNE.setWorldborderNeighbor(N);
    cNE.setWorldborderNeighbor(E);
    cNE.setWorldborderNeighbor(NE);
    cNE.setWorldborderNeighbor(NW);
    cNE.setWorldborderNeighbor(SE);

    cE.setWorldborderNeighbor(E);
    cE.setWorldborderNeighbor(NE);
    cE.setWorldborderNeighbor(SE);

    cSE.setWorldborderNeighbor(S);
    cSE.setWorldborderNeighbor(E);
    cSE.setWorldborderNeighbor(SE);
    cSE.setWorldborderNeighbor(NE);
    cSE.setWorldborderNeighbor(SW);

    cS.setWorldborderNeighbor(S);
    cS.setWorldborderNeighbor(SW);
    cS.setWorldborderNeighbor(SE);

    cSW.setWorldborderNeighbor(S);
    cSW.setWorldborderNeighbor(W);
    cSW.setWorldborderNeighbor(SW);
    cSW.setWorldborderNeighbor(NW);
    cSW.setWorldborderNeighbor(SE);

    cW.setWorldborderNeighbor(W);
    cW.setWorldborderNeighbor(NW);
    cW.setWorldborderNeighbor(SW);

    cNW.setWorldborderNeighbor(N);
    cNW.setWorldborderNeighbor(W);
    cNW.setWorldborderNeighbor(NW);
    cNW.setWorldborderNeighbor(SW);
    cNW.setWorldborderNeighbor(NE);

    //put objects
    cNW.setCell(2, 2, true);
    cNE.setCell(6, 2, true);
    cSW.setCell(2, 6, true);
    cSE.setCell(6, 6, true);

    cN.setCell(3, 2, true);
    cN.setCell(5, 2, true);

    cE.setCell(6, 3, true);
    cE.setCell(6, 5, true);

    cS.setCell(3, 6, true);
    cS.setCell(5, 6, true);

    cW.setCell(2, 3, true);
    cW.setCell(2, 5, true);

    /*
    cC.setCell(3, 3, true);
    cC.setCell(5, 3, true);
    cC.setCell(5, 5, true);
    cC.setCell(3, 5, true);
    */

    cN.synchronizeBorder();
    cNE.synchronizeBorder();
    cE.synchronizeBorder();
    cSE.synchronizeBorder();
    cS.synchronizeBorder();
    cSW.synchronizeBorder();
    cW.synchronizeBorder();
    cNW.synchronizeBorder();
    cC.synchronizeBorder();

    cN.tickCells();
    cNE.tickCells();
    cE.tickCells();
    cSE.tickCells();
    cS.tickCells();
    cSW.tickCells();
    cW.tickCells();
    cNW.tickCells();
    cC.tickCells();

    //see if corners of center cell came to life
    REQUIRE(cC.isAlive(3, 3));
    REQUIRE(cC.isAlive(5, 3));
    REQUIRE(cC.isAlive(5, 5));
    REQUIRE(cC.isAlive(3, 5));
}

