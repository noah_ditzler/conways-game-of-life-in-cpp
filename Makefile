CXX = g++
CXXFLAGS = -g -std=c++20 -lpthread

OBJS = world
OBJS += chunk
OBJS += sequential_world
OBJS += concurrent_world
OBJS += renderer
OBJS += ascii_renderer

_OBJS := $(foreach obj, $(OBJS), .objs/$(obj).o)


collage: $(OBJS)
	$(CXX) $(CXXFLAGS) src/main.cpp $(_OBJS) -o gol

build_gol_test: $(OBJS)
	$(CXX) $(CXXFLAGS) test/test.cpp $(_OBJS) -o .gol_test

test : build_gol_test
	./.gol_test

%:
	$(CXX) $(CXXFLAGS) -Wall -c src/$@.cpp -o .objs/$@.o
	
clean:
	rm .objs/* gol .gol_test
